package com.jeeranan132.week10;

public class Circle extends Shape {
    private double redius;

    public Circle(double redius) {
        super("circle");
        this.redius = redius;
    }

    @Override
    public double calArea() {
        return Math.PI * this.redius * this.redius;
    }

    @Override
    public double calPerimeter() {
        return 2 * Math.PI * this.redius;
    }

    @Override
    public String toString() {
        return this.getName() + " redius:" + this.redius;
    }

}
