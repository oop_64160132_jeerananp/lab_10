package com.jeeranan132.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.printf("%s area: %.0f \n", rec.getName(), rec.calArea());
        System.out.printf("%s perimeter: %.0f \n", rec.getName(), rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("%s area: %.0f \n", rec2.getName(), rec2.calArea());
        System.out.printf("%s perimeter: %.0f \n", rec2.getName(), rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area: %.3f \n", circle1.getName(), circle1.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle1.getName(), circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n", circle1.getName(), circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle1.getName(), circle2.calPerimeter());

        Triangle tri1 = new Triangle(5, 5, 6);
        System.out.println(tri1);
        System.out.printf("%s area: %.3f \n", tri1.getName(), tri1.calArea());
        System.out.printf("%s perimeter: %.3f \n", tri1.getName(), tri1.calPerimeter());

        Triangle tri2 = new Triangle(4, 4, 5);
        System.out.println(tri2);
        System.out.printf("%s area: %.3f \n", tri2.getName(), tri2.calArea());
        System.out.printf("%s perimeter: %.3f \n", tri2.getName(), tri2.calPerimeter());
    }
}
